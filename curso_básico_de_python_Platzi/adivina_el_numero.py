import random

def main():
    numero_aleatorio = random.randint(1, 100)
    numero_elegido = int(input('Eige un número entre 1 y 100: ')) 

    while numero_elegido != numero_aleatorio:
        if numero_elegido < numero_aleatorio:
            print('Busca un número más grande')
        else:
            print('Busca un número más pequeño')
        
        numero_elegido = int(input('Eige otro número: ')) 

    print('Ganaste!')


if __name__ == '__main__':
    main()
