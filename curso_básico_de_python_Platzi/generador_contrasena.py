import random

MAYUSCULAS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z']
MINUSCULAS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z']
DIGITOS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
SIMBOLOS = ['*', '+', '-', '/', '@', '_', '?', '!', '[', '{', '(', ')', '}', ']', ',', ';', '.', '>', '<', '&', '$', '#']

def generar_contrasena():
    caracteres = MAYUSCULAS + MINUSCULAS + DIGITOS + SIMBOLOS

    contrasena = []
    for i in range(15):
        caracter_random = random.choice(caracteres)
        contrasena.append(caracter_random)
    
    return ''.join(contrasena)


def main():
    contrasena = generar_contrasena()
    print('Tu nueva contrasena es: ' + contrasena)

if __name__ == '__main__':
    main()
