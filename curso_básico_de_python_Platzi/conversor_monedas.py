def conversor(tipo_pesos, valor_dolar):
    pesos = input("¿Cuántos pesos " + tipo_pesos + " tienes?: ")
    pesos = float(pesos)
    dolares = pesos / valor_dolar
    dolares = round(dolares, 2)
    dolares = str(dolares)
    print("Tienes $" + dolares + " USD")

menu = """"
Bienvenido al conversor de monedas 💰

1 - Pesos COL
2 - Pesos ARG
3 - Pesos MX

Elige una opcion: """

opcion = int(input(menu))

if opcion == 1:
    conversor("COL", 3979.33)
elif opcion == 2:
    conversor("ARS", 118.017)
elif opcion == 3:
    conversor("MX", 19.86)
else:
    print("Ingresa una opción correcta")
