from functools import reduce
import os
import random

def pick_word():
    with open("./archivos/data.txt", "r", encoding="utf-8") as f:
        words = f.read().splitlines()
    
    return words[random.randrange(len(words))]


def draw_hangman(drawable_chars, word_to_guess):
    os.system("clear")
    print("¡Adivina la palabra!")
    print("--------------------")
    for i, is_drawable in enumerate(drawable_chars):
        if is_drawable:
            print(word_to_guess[i].lower(), end="")
        else:
            print("_", end="")
    print("")


def guess_word(input, word_to_guess, drawable_chars):
    for i, char in enumerate(word_to_guess):
        if char == input:
            drawable_chars[i] = True


def main():
    word_to_guess = pick_word()
    drawable_chars = [False] * len(word_to_guess)
    while reduce(lambda x, y: x and y, drawable_chars) is False:
        draw_hangman(drawable_chars, word_to_guess)
        new_letter = input("Ingresa una letra: ").lower()
        guess_word(new_letter, word_to_guess, drawable_chars)
    
    print("¡Ganaste! La palabra era:", word_to_guess)

if __name__ == "__main__":
    main()
